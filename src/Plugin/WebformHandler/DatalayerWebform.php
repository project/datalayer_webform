<?php

namespace Drupal\datalayer_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Datalayer webform handler.
 *
 * @WebformHandler(
 *   id = "datalayer_webform",
 *   label = @Translation("Datalayer Webform"),
 *   category = @Translation("Datalayer Webform"),
 *   description = @Translation("IMPORTANT: Tested only with modal confirmation behavior."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class DatalayerWebform extends WebformHandlerBase {

  /**
   * Event data with replaced tokens.
   *
   * @var string|array<mixed>
   */
  private $eventData;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'event' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Config'),
    ];
    $form['config']['event'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Datalayer event. (YAML)'),
      '#default_value' => $this->configuration['event'],
    ];
    $form['config']['token_tree_link'] = $this->buildTokenTreeElement();
    $this->elementTokenValidate($form);
    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    if (!empty($this->eventData)) {
      $form['#attached']['library'][] = 'datalayer_webform/event';
      $form['#attached']['drupalSettings']['datalayer_webform']['event'] = $this->eventData;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    if ($this->configuration['event']) {
      $data = Yaml::decode($this->configuration['event']);
      $this->eventData = $this->replaceTokens($data, $webform_submission);
    }
  }

}
