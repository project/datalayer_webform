/**
 * @file
 * JavaScript behaviors for datalayer webform event.
 */
(function (Drupal, once) {

  Drupal.behaviors.datalayerWebformEvent = {
    attach: function (context, settings) {
      // @NOTE For now just one per webform.
      once('datalayer-webform-event', '.webform-submission-form', context).forEach(function () {
        if (settings.datalayer_webform !== undefined) {
          dataLayer.push(settings.datalayer_webform.event);
        }
      });
    }
  };

})(Drupal, once);
